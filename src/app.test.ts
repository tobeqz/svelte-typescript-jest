import App from "./App.svelte";
import { render } from "@testing-library/svelte";

it("Test name rendering", async () => {
    let component = render(App, {
        name: "lol",
    });

    expect(await component.findByText("Hello lol!")).toBeTruthy();
});
