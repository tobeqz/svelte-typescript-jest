import Button from "./Button.svelte";
import { render, fireEvent } from "@testing-library/svelte";

test("Button should render buttontext", async () => {
    const comp = render(Button, {
        buttonText: "coolio",
    });

    const button = await comp.findByText("coolio");

    expect(button.textContent).toBe("coolio");
});

test("Button should react to click", async () => {
    const comp = render(Button, {
        buttonText: "a",
    });

    let clicked = false;

    comp.component.$on("click", () => (clicked = true));
    const button = await comp.findByText("a");

    fireEvent(button, new MouseEvent("click"));

    expect(clicked).toBe(true);
});

test("Button should rerender", async () => {
    const comp = render(Button, {
        buttonText: "b"
    })

    expect(comp.container.textContent).toBe('b')

    comp.rerender({
        buttonText: "c"
    })

    expect(comp.container.textContent).toBe("c")
})
