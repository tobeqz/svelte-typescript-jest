import Counter from "./Counter.svelte";
import { render, fireEvent } from "@testing-library/svelte";

test("Should render 2 buttons with starting value 6", async () => {
    const comp = render(Counter, {
        value: 6,
    });

    const textContent = comp.container.getElementsByTagName("p")[0].textContent;

    expect(textContent).toBe("Value is: 6");
});

test("Test if buttons work", async () => {
    const comp = render(Counter, {
        value: 6,
    });

    const incButton = comp.getByText("+");
    const decButton = comp.getByText("-");

    await fireEvent(incButton, new Event("click"));
    await fireEvent(incButton, new Event("click"));
    await fireEvent(decButton, new Event("click"));

    const textContent = comp.container.querySelector("p").textContent;

    expect(textContent).toBe("Value is: 7");
});
