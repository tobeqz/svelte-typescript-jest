module.exports = {
    transform: {
        '^.+\\.svelte$': [
            'svelte-jester',
            {
                "preprocess": true
            }
        ],
        '^.+\\.ts$': 'ts-jest',
        '^.+\\.js$': 'babel-jest'
    },

    moduleFileExtensions: ["ts", "js", 'svelte']
}